<?php
namespace Mksav\Presentable;

interface PresentableInterface
{
    /**
     * Return a new instance of a given presenter
     */
    public function getPresenter();
}
