<?php
namespace Mksav\Presentable;

use IteratorAggregate;
use ArrayIterator;

class CollectionPresenter implements IteratorAggregate
{
    /**
     * The collection
     * 
     * @var mixed Must be iterable in some way
     */
    protected $collection;

    /**
     * New collection of all entities in the collection after being decorated
     * 
     * @var array
     */
    protected $entities = [];


    public function __construct($collection)
    {
        $this->collection = $collection;

        $this->entities = $this->decorate($collection);
    }

    /**
     * Decorate each member of the collection, by creating an instance of the Presenter for the member
     * or by using the member itself, and return the resulting array
     * 
     * @param  mixed
     * @return array
     */
    public function decorate($collection)
    {
        $entities = [];
        foreach ($collection as $entity) {
            $entities[] = (($entity instanceof PresentableInterface) ? $entity->getPresenter() : $entity);
        }

        return $entities;
    }

    /**
     * Return the entities
     * 
     * @return array
     */
    public function getEntities()
    {
        return $this->entities;
    }

    /**
     * Return the original collection instance
     * 
     * @return mixed
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Implementation of the IteratorAggregate interface
     * 
     * @return array
     */
    public function getIterator()
    {
        return new ArrayIterator($this->entities);
    }
}
