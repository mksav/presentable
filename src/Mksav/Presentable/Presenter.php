<?php
namespace Mksav\Presentable;

use BadMethodCallException;

abstract class Presenter
{
    /**
     * The object behind the presenter
     * 
     * @var mixed
     */
    protected $model;

    /**
     * Pass in model instance
     * 
     * @param  mixed
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Return the model instance
     * 
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Return a property from the original model instance, or, call a 
     * method defined in the model's presenter
     * 
     * @param  string
     * @return mixed
     */
    public function __get($field)
    {        
        return ((method_exists($this, $field)) 
            ? $this->$field()
            : $this->model->$field);
    }

    /**
     * Magic method to handle method calls on the presenter instance OR the model instance
     * 
     * @throws BadMethodCallException
     */
    public function __call($method, $args)
    {
        if (method_exists($this, $method))
            return call_user_func_array([$this, $method], $args);
        elseif (method_exists($this->model, $method))
            return call_user_func_array([$this->model, $method], $args);

        throw new BadMethodCallException;
    }
}
